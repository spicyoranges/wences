# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Prepopulate categories
Category.create(name: 'Departamento')
Category.create(name: 'Casa')
Category.create(name: 'Duplex')
Category.create(name: 'PH')
Category.create(name: 'Loft')
Category.create(name: 'Local Comercial')
Category.create(name: 'Oficina')
